#!/bin/sh
portagedir="/etc/portage"
patchdir="${portagedir}/patches"
confdir="${portagedir}/savedconfig"

write () {
    hd='\033[0;31m'
    ed='\033[0m'
    >&2 echo -e "${hd}${1}${ed}${2}"
}

mkpath () {
    tm=`date '+%H%M%S'`
    fullpath="${1}/${2}"
    if [ -e "$fullpath" ]; then
        if [ ! -d "$fullpath" ]; then
            write "Moved file $fullpath" " to /tmp/$tm"
            mv $fullpath /tmp/$tm && mkdir -p $fullpath
        fi
    else
        mkdir -p $fullpath
    fi
}

do_patch() {
    mkpath $patchdir $path && echo $fullpath
    find "./$1" -name '*.diff' -exec cp -v {} $fullpath \;
}
do_config() {
    path="${path%%/*}"
    mkpath $confdir $path && echo $fullpath
    for i in "$(ls ./$1/*.h)"; do
        cp -v $i "${fullpath}/$(basename -s'.h' $i)"
    done
}


# kernel
write "Installing kernel patches" && path="sys-kernel/gentoo-kernel"
do_patch kernel

# dmenu
write "Installing dmenu patches" && path="x11-misc/dmenu"
do_patch dmenu
write "Installing dmenu config" && path="${path##*/}"
do_config dmenu

# st
write "Installing st patches" && path="x11-terms/st"
do_patch st
write "Installing st config"
do_config st
